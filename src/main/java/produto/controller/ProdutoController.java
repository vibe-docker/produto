package produto.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import produto.model.Produto;

@RestController
@RequestMapping(path = "/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ProdutoController {
	
	@GetMapping("/produtos")
	public List<Produto> produtos() throws Exception {
		
		List<Produto> produtos = new ArrayList<Produto>();
		
		produtos.add(new Produto(1L, "Produto 1"));
		produtos.add(new Produto(2L, "Produto 2"));
		produtos.add(new Produto(2L, "Produto 3"));
		
		return produtos;
	}
	@GetMapping("/v2/produtos")
	public List<Produto> produtosV2() throws Exception {
		
		List<Produto> produtos = new ArrayList<Produto>();
		
		produtos.add(new Produto(1L, "Produto 1"));
		produtos.add(new Produto(2L, "Produto 2"));
		produtos.add(new Produto(2L, "Produto 3"));
		
		return produtos;
	}

}
